// Include files

#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <map>
#include "TString.h"
#include "Structs.h"

using namespace std;

/** @class Parser Parser.h macros/Parser.h
 *   *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2011-07-12
 */


class Parser {
public: 
  typedef std::map<std::string, Param_Result*> Param_Result_Map;
  typedef std::map<std::string, XYZ_Pos*> XYZ_Pos_Map;

  /// Standard constructor
  Parser() {}; 
  virtual ~Parser() {}; ///< Destructor
  
  Parser::Param_Result_Map* CreateParamResultMapFromFitFile(TString const& filename);
  Parser::XYZ_Pos_Map* CreateXYZPosMapFromFile(TString const& filename);
  
protected:

private:
  
  //ClassDef(Parser,1);
};

#endif
